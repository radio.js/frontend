import WebAPI from '../../services/WebAPI';
import interact from 'interactjs';
import { throttle } from 'lodash';
import {bindable, inject} from 'aurelia-framework';

@inject(WebAPI)
export class PlaybackBar {
  @bindable isPaused;
  @bindable songName;
  @bindable radioName;
  @bindable volume;

  // Is the volume slider open
  isVolumeSliderOpen = false;

  // Is the volume slider open and draggable
  isSliderInitialized = false;

  // Is the user dragging the slider
  isDragging = false;

  // Is the timeout for ending the drag running
  isDragStopping = false;

  // Is the slider being toggled between open an closed
  isTogglingSlider = false;

  constructor(webAPI) {
    this.webAPI = webAPI;
  }

  togglePause() {
    if (this.isPaused) {
      this.webAPI.play();
    } else {
      this.webAPI.pause();
    }

    this.isPaused = !this.isPaused;
  }

  toggleVolumeSlider() {
    if (this.isDragging || this.isTogglingSlider) return;
    this.isTogglingSlider = true;

    if (!this.isVolumeSliderOpen) {
      this.isVolumeSliderOpen = !this.isVolumeSliderOpen;
      setTimeout(() => {
        this.initializeSlider();
        setTimeout(() => {
          this.isSliderInitialized = true;
          this.isTogglingSlider = false;
        }, 210);
      }, 210);
    } else {
      this.isSliderInitialized = false;
      this.volumeSliderHandle.style.transform = '';
      setTimeout(() => {
        this.isVolumeSliderOpen = !this.isVolumeSliderOpen;
        setTimeout(() => {
          this.slider.unset();
          this.isTogglingSlider = false;
          this.isDragging = false;
        }, 210);
      }, 210);
    }

  }

  initializeSlider() {
    this.updateSliderOffset();

    this.slider = interact(this.volumeSlider);
    this.slider.draggable({
      origin: 'self',
      inertia: true,
      modifiers: [
        interact.modifiers.restrict({
          restriction: 'self '
        }),
      ],
      startAxis: 'x',
      lockAxis: 'x',
      listeners: {
        start: () => {
          this.isDragging = true;
          this.isDragStopping = false;
        },
        move: (event) => {
          if (!this.isSliderInitialized) return;

          const { handleWidth, handleOffset, sliderWidth } = this.getSliderInfo();
          const mouseLocation = Math.max(handleOffset, Math.min(event.pageX, sliderWidth - handleOffset));
          const volumeValue = Math.max(0, Math.min((mouseLocation - handleOffset) / (sliderWidth - handleWidth), 1));
          this.volume = volumeValue * 100;

          this.updateSliderOffset();

          this.throttleSetVolume(Math.round(this.volume));
        },
        end: () => {
          this.isDragStopping = true;
          setTimeout(() => {
            if (this.isDragStopping) {
              this.isDragging = false;
              this.isDragStopping = false;
            }
          }, 100);
        },
      }
    }).styleCursor(false);
  }

  updateSliderOffset() {
    const sliderWidth = this.volumeSlider.offsetWidth;
    const handleWidth = this.volumeSliderHandle.offsetWidth;
    const value = (this.volume * (sliderWidth - handleWidth)) / 100;
    this.volumeSliderHandle.style.transform = `translateX(${value}px)`;
  }

  getSliderInfo() {
    return {
      handleWidth: this.volumeSliderHandle.offsetWidth,
      handleOffset: this.volumeSliderHandle.offsetWidth / 2,
      sliderWidth: this.volumeSlider.offsetWidth,
    };
  }

  volumeChanged(oldVolume, newVolume) {
    if (oldVolume !== newVolume && this.isVolumeSliderOpen && !this.isDragging) {
      this.updateSliderOffset();
    }
  }

  throttleSetVolume = throttle((volume) => {
    this.webAPI.setVolume(volume);
  }, 100);
}
