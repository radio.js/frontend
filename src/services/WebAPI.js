import {HttpClient, json} from 'aurelia-fetch-client';
import RestartableEventSource from "../helper/RestartableEventSource";

export default class WebAPI {
  httpClient = new HttpClient();
  sse = new RestartableEventSource('http://localhost:1337/events');

  constructor() {
    this._configureHttpClient();
    this._setupErrorHandler();
  }

  _configureHttpClient() {
    this.httpClient.configure(config => {
      config.useStandardConfiguration()
        .withBaseUrl('http://localhost:1337/');
    });
  }

  _setupErrorHandler() {
    this.sse.addEventListener('error', () => {
      console.error('Error in the event source, reconnecting in 2 seconds...');
      setTimeout(() => {
        this.sse.restart();
      }, 2000);
    });

    this.sse.addEventListener('open', () => {
      console.log('Connected to event source');
    })
  }

  async getRadioList() {
    const response = await this.httpClient.fetch('radio');
    return response.json();
  }

  async getStatus() {
    const response = await this.httpClient.fetch('status');
    return response.json();
  }

  async changeRadio(radio) {
    const radioId = radio.id;
    await this.httpClient.post('playback/change-radio', json({id: radioId}));
  }

  async setVolume(volume) {
    await this.httpClient.post('playback/volume', json({volume: volume}));
  }

  async play() {
    await this.httpClient.post('playback/play');
  }

  async pause() {
    await this.httpClient.post('playback/pause');
  }

  async getRadio(radioId) {
    const response = await this.httpClient.get(`radio/${radioId}`);
    return response.json();
  }

  async newRadio(radio) {
    return await this.httpClient.post('radio', json(radio));
  }

  async modifyRadio(radio) {
    return await this.httpClient.patch(`radio/${radio.id}`, json(radio));
  }

  async deleteRadio(radio) {
    return await this.httpClient.delete(`radio/${radio.id}`)
  }
}
