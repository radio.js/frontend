import StorageService from "./StorageService";
import {inject} from "aurelia-dependency-injection";
import {observable} from "aurelia-binding";

@inject(StorageService)
export default class NotificationService {

  @observable
  enabled = false;
  denied = false;

  /**
   * @param {StorageService} storageService
   */
  constructor(storageService) {
    this.storage = storageService;

    this.enabled = this.storage.get('notifications-enabled', false);
    this.denied = Notification.permission === 'denied';
  }

  toggleNotifications() {
    if (this.enabled) {
      this.enabled = false;
    } else {
      this.enableNotifications();
    }
  }

  sendNotification(title, body) {
    if (!document.hasFocus() && this.enabled) {
      new Notification(title, { body });
    }
  }

  enableNotifications() {
    Notification.requestPermission().then(this.handlePermission.bind(this))
  }

  handlePermission() {
    if (Notification.permission === 'granted') {
      this.enabled = true;
      this.denied = false;
    } else {
      this.enabled = false;
      this.denied = true;
    }
  }

  enabledChanged() {
    this.storage.set('notifications-enabled', this.enabled);
  }
}
