import {inject} from "aurelia-dependency-injection";
import {observable} from "aurelia-binding";
import NotificationService from "./NotificationService";
import WebAPI from "./WebAPI";

@inject(WebAPI, NotificationService)
export default class PlayerService {
  #lastRealSongName = '';

  isPaused = false;
  @observable songName = '';
  radioName = '';
  volume = 0;

  /**
   * @param {WebAPI} webAPI
   * @param {NotificationService} notificationService
   */
  constructor(webAPI, notificationService) {
    this.webAPI = webAPI;
    this.notificationService = notificationService;

    this.addSseListeners();
    this.setInitialStatus();
  }

  setInitialStatus() {
    this.webAPI.getStatus().then(result => {
      this.eventSongName(result.songTitle);
      this.eventVolume(Number.parseInt(result.volume, 10));
      this.eventState(result.state !== 'play');
      this.eventRadioChanged(result.radio);
    })
  }

  addSseListeners() {
    this.webAPI.sse.addEventListener('song_name', event => this.eventSongName(event.data));
    this.webAPI.sse.addEventListener('volume', event => this.eventVolume(Number.parseInt(event.data, 10)));
    this.webAPI.sse.addEventListener('state', event => this.eventState(event.data !== 'play'));
    this.webAPI.sse.addEventListener('radio', event => this.eventRadioChanged(JSON.parse(event.data)));
  }

  eventSongName(songName) {
    this.songName = songName;
  }

  eventVolume(volume) {
    this.volume = volume;
  }

  eventState(isPaused) {
    this.isPaused = isPaused;
  }

  eventRadioChanged(radio) {
    this.radioName = radio !== null ? radio.name : '';
  }

  songNameChanged() {
    if (this.songName !== '') {
      if (this.#lastRealSongName !== '' && this.#lastRealSongName !== this.songName) {
        this.notificationService.sendNotification(this.songName, this.radioName);
      }

      this.#lastRealSongName = this.songName;
    }
  }
}
