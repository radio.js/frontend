export default class StorageService {
  constructor() {
    this.storage = localStorage;
  }

  get(key, defaultValue) {
    const value = this.storage.getItem(key);
    return value !== null ? JSON.parse(value) : defaultValue;
  }

  set(key, value) {
    const jsonValue = JSON.stringify(value);
    this.storage.setItem(key, jsonValue);
  }

  remove(key) {
    this.storage.removeItem(key);
  }
}
