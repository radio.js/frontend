import('line-awesome/dist/line-awesome/scss/line-awesome.scss');

export class App {
  configureRouter(config, router) {
    config.title = 'radio.js';
    config.options.pushState = true;
    config.options.root = '/';
    config.map([
      { route: '',          name: 'home', moduleId: PLATFORM.moduleName('components/home/home') },
    ]);
  }
}
