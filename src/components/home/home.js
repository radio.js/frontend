import {inject} from 'aurelia-dependency-injection';
import WebAPI from '../../services/WebAPI';
import PlayerService from "../../services/PlayerService";

@inject(WebAPI, PlayerService)
export class Home {
  radios = [];

  /**
   *
   * @param {WebAPI} webAPI
   * @param {PlayerService} playerService
   */
  constructor(webAPI, playerService) {
    this.webAPI = webAPI;
    this.player = playerService;
  }

  created() {
    this._setupInitialState();
    this._addSseListeners();
  }

  _setupInitialState() {
    this._loadRadioList();
  }

  _loadRadioList() {
    this.webAPI.getRadioList().then(radios => {
      this.radios = radios;
      this._sortRadios();
    });
  }

  _addSseListeners() {
    this.webAPI.sse.addEventListener('open', () => this._setupInitialState());

    this.webAPI.sse.addEventListener('create', event => this.eventCreate(JSON.parse(event.data)));
    this.webAPI.sse.addEventListener('update', event => this.eventUpdate(JSON.parse(event.data)));
    this.webAPI.sse.addEventListener('delete', event => this.eventDelete(JSON.parse(event.data)));

  }

  _sortRadios() {
    this.radios.sort((a, b) => ('' + a.name).localeCompare(b.name));
  }

  eventCreate(radio) {
    this.radios.push(radio);
    this._sortRadios();
  }

  eventUpdate(radio) {
    const indexInList = this.radios.findIndex(x => x.id === radio.id);
    if (indexInList === -1) {
      return;
    }

    this.radios.splice(indexInList, 1, radio);
    this._sortRadios();
  }

  eventDelete(radio) {
    const indexInList = this.radios.findIndex(x => x.id === radio.id);
    if (indexInList === -1) {
      return;
    }

    this.radios.splice(indexInList, 1);
    this._sortRadios();
  }


}
